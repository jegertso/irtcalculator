from setuptools import setup, find_packages
from subprocess import check_output


def get_version_string():
    output = check_output(["git", "describe", "--tags"])
    parts = output.split('-')
    tag, count, sha = parts[:3]
    return "{}.dev{}+{}".format(tag, count, sha)

setup(
    name="IRTCalculator",
    version=get_version_string(),
    description="Generate and irtdb from a Skyline report",
    author="Jarrett Egertson",
    author_email="jegertso@uw.edu",
    classifiers=['Development Status :: 3 - Alpha',
                 'Programming Language :: Python :: 2.7'],
    zip_safe=False,
    packages=find_packages(),
    install_requires=['pandas',
                      'numpy',
                      'sqlalchemy'],
)

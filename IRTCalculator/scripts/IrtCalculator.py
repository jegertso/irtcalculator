#!/usr/bin/env python

"""
IrtCalculator.py
Original Author: Jarrett Egertson

This script reads in a skyline report and builds an irt library from it.
"""

import argparse
import os
import logging
import pandas as pd
from sqlalchemy import create_engine


def init_argparse():
    parser = argparse.ArgumentParser(
        description="IrtCalculator reads a flat file (.csv) with peptides and retention time values and generates an "
                    "irt database. The flat file may be a Skyline report. It should have columns for Peptide Modified "
                    "Sequence, File Name, Protein Name, and Peptide Retention Time",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output irt library if it already exists")
    parser.add_argument('--calibrant', default='PRTC', help="Name of calibrant protein to use as irt standard or "
                                                            "specify a .csv file which contains report info for just "
                                                            "the calibrant peptides")
    parser.add_argument('inputReport', type=os.path.abspath, help="Input report (.csv) file")
    parser.add_argument('outputDatabase', type=os.path.abspath, help="Output irt database file ('.irtdb')")
    return parser


def calibrate_irt(input_df):
    """
    Calculate irt values for calibrants based on observed retention times in a single run
    :param input_df: data frame with "Peptide Modified Sequence" and "Peptide Retention Time" entries for calibrant
     peptides derived from a single instrument run
    :return: a Series with index Peptide Modified Sequence and column irt
    """
    out_df = input_df.set_index('Peptide Modified Sequence', check_integrity=True)
    min_rt = out_df['Peptide Retention Time'].min()
    max_rt = out_df['Peptide Retention Time'].max()
    out_df['irt'] = out_df['Peptide Retention Time'].apply(lambda x: (x - min_rt) * (max_rt - min_rt)/100.0)
    return out_df['irt']


def main():
    # read Skyline report with PeptideModSeq, FileName, Protein Name (to indicate standard)
    logging.basicConfig(level=logging.INFO)
    parser = init_argparse()
    args = parser.parse_args()
    if os.path.exists(args.outputDatabase) and not args.overwrite:
        raise Exception("Output irt database file %s already exists! Aborting. Did you mean to specify --overwrite?"
                        % args.outputDatabase)

    # read the input file
    df = pd.read_csv(args.inputReport)
    df['Standard'] = 0

    # generate a dataframe with info from the calibrants
    if args.calibrant.endswith('.csv'):
        # a separate calibrant dataframe was given
        calibrant_df = pd.read_csv(args.calibrant)
        calibrant_df['Standard'] = 1
        df = pd.concat([df, calibrant_df])
    else:
        df[df['Protein Name'] == args.calibrant]['Standard'] = 1

    # calculate calibrated iRT values for each training run
    irt_per_run = df[df['Standard'] == 1].groupby('File Name').apply(calibrate_irt)
    # average the calibrated iRT values
    print irt_per_run

if __name__ == '__main__':
    main()
